<?php

namespace Drupal\living_word_restful\Controller;

use Drupal\Core\Database\Database;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Class RESTController.
 *
 * @package Drupal\living_word_restful\Controller
 */
class RESTController extends ControllerBase {
  public function translations() {   # return an array of all the installed translations - code+shortnames
    $dbconn = Database::getConnection();
    $results = $dbconn->query("SELECT abbr, name FROM lw_translations")->fetchAll();
    return new JsonResponse($results);
  }

  public function booknames($translation) {
    $dbconn = Database::getConnection();
    if ($translation=="") {
      //default, pick the first installed translation
      $translation = $dbconn->query("SELECT abbr FROM lw_translations LIMIT 1")->fetchCol();
      $translation = $translation[0];
    } 
    //we need to return three parts:   
	//1. Booknames
	//2. Versemaps
	//3. Abbreviations for booknames
    $names = $dbconn->query("
      SELECT booknum, bookname 
      FROM lw_books
      WHERE translation=:trans", 
      array(
        ":trans" => $translation
      ) )->fetchAll();
	if (!$names) {
		$names = "Error! Translation $translation is not installed!";
	} else {
		$versemap = $dbconn->query("
		   SELECT booknum, chapter, versemap, vidmap
		   FROM lw_chapterversemap 
		   WHERE translation=:trans 
		   ORDER BY booknum, chapter;", [":trans"=>$translation])->fetchAll();
		$lang = $dbconn->query("SELECT lang FROM lw_translations WHERE abbr=:trans", [":trans"=>$translation])->fetchField();
		
		$abbrs = $dbconn->query("SELECT booknum, COALESCE(
			  GROUP_CONCAT(CASE WHEN lang=:lang and weight=0 THEN abbr END), 
			  GROUP_CONCAT(CASE WHEN lang='und' and weight=0 THEN abbr END)) AS Abbr,
			  GROUP_CONCAT(abbr) as AltAbbr
			FROM lw_bookabbr 
			WHERE (lang=:lang or lang='und')
			GROUP BY booknum;", [":lang"=>$lang])->fetchAll();
	// SELECT lang FROM lw_bookabbr WHERE lang='en' OR lang='und';
		$j = 0;
		$a = 0;
		for ($i = 0; $i < count($names); $i++) {
			# copy the preferred abbr and alternative abbrs
			while ($abbrs[$a]->booknum < $names[$i]->booknum) $a++;   # for translations without all the books
			$names[$i]->Abbr =    $abbrs[$a]->Abbr;
			$names[$i]->AltAbbr = $abbrs[$a]->AltAbbr;
			for ($k=$j; ($k < count($versemap)) && ($versemap[$k]->booknum==$names[$i]->booknum) ; $k++) {}
			
			# also insert the versemaps, from $j to $k-1
			$vmbook = array_slice ($versemap, $j, $k-$j);
			# simplify -> remove the booknum and chapter number
			foreach($vmbook as $nr => &$vm) {
				unset($vm->booknum);
				if ($nr==$vm->chapter-1) {
					unset ($vm->chapter);
				}
			}
			$names[$i]->versemap = $vmbook;
			$j = $k;
			$a++;
		}
	}
	
    $res = new JsonResponse($names);
    $res->setEncodingOptions(JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_NUMERIC_CHECK);
    return $res;
  }

  public function scripture($trans, $startvid, $endvid) {
    $dbconn = Database::getConnection();
	if ($endvid==0) {
		$endvid = $startvid + 200000;
	}
    $results = $dbconn->query("
      SELECT vid, booknum, chapternum, versenum, versetext  
      FROM lw_verses
      WHERE (translation=:trans) AND (vid >=:startvid) AND (vid <=:lastvid)
      ORDER BY vid 
      LIMIT 50" , 
      array(
        ":trans" => $trans,
        ":startvid" => $startvid,
        ":lastvid" => $endvid
      ) )->fetchAll();
    return new JsonResponse($results);
  }

  public function commentary($level, $startvid, $endvid, $page) {
    $dbconn = Database::getConnection();
	$results = array("hierso kom die brokkies");
    $res = new JsonResponse($results);
    $res->setEncodingOptions(JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_NUMERIC_CHECK);
    return $res;
  }
}
